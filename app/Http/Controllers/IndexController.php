<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Info;

class IndexController extends Controller
{
    public function index()
    {
        $info = Info::all();
        return view('welcome', [
            'data' => $info,
        ]);
    }
}
